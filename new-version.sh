#!/bin/bash
newVersion=$2

# Get the last branch
lastGitBranch=$1

#  Checkout it
git checkout $lastGitBranch

rm -rf LaravelApp

# Install Laravel
php $(which composer) create-project --prefer-dist laravel/laravel LaravelApp $newVersion

# Remove lock file, it is irrelevant for computing the diff
rm LaravelApp/composer.lock

# Create the new branch
git branch laravel-$newVersion

# Checkout it
git checkout laravel-$newVersion

# Add modified files to commit
git add LaravelApp

# Commit
git commit -m "Output version ${newVersion}"
